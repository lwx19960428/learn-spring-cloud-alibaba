package cn.snowheart.cloud.learn.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuwanxiang
 * @version 2019/04/19
 */
@Slf4j
@RestController
public class HelloWorldController {

    @GetMapping("/hello/world")
    public String helloWorld(){
        return "Hello World!";
    }

}
