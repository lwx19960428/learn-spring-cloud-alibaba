package cn.snowheart.cloud.learn.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuwanxiang
 * @version 2019/04/15
 */
@RestController
@Slf4j
public class HelloController {

    @GetMapping("/hello")
    public String hello(@RequestParam("name") String name) {
        log.info("invoked name is {}", name);
        return "hello " + name;
    }

}
