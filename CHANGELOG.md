# 2019-04-22

1. 更新Spring Cloud Alibaba版本到`0.9.0.RELEASE`，增加`spring.cloud.sentinel.datasource.ds.nacos.rule-type`配置项
2. 本想补充Sentinel-Apollo章节内容，始终没有运行成功，待后续运行成功后，再次提交代码
3. 增加端口规划章节

# 2019-04-19

1. 初始化提交，提交NacOS学习的部分内容
2. 下午补充Sentinel学习内容