package cn.snowheart.cloud.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author liuwanxiang
 * @version 2019/04/15
 */
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class CommonClientApplication {

    public static void main(String[] args) {

        SpringApplication.run(CommonClientApplication.class, args);

    }

}
