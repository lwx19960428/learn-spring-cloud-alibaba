package cn.snowheart.cloud.learn.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author liuwanxiang
 * @version 2019/04/15
 */
@RestController
@Slf4j
public class TestController {

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @GetMapping("/test/normal")
    public String test() {

        ServiceInstance serviceInstance = loadBalancerClient.choose("alibaba-nacos-discovery-server");
        String url = serviceInstance.getUri() + "/hello?name=alibaba-nacos-discovery-client-common";
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(url, String.class);
        return "Invoke : " + url + ", response : " + response;

    }

}
