package cn.snowheart.cloud.learn.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author liuwanxiang
 * @version 2019/04/15
 */
@FeignClient("alibaba-nacos-discovery-server")
public interface ServerClient {

    /**
     * Feign-远程RPC的接口
     *
     * @param name
     * @return
     */
    @GetMapping("/hello")
    String hello(@RequestParam(name = "name") String name);

}
