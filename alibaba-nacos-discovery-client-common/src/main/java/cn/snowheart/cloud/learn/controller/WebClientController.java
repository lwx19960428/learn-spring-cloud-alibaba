package cn.snowheart.cloud.learn.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * @author liuwanxiang
 * @version 2019/04/15
 */
@Slf4j
@RestController
public class WebClientController {

    @Autowired
    private WebClient.Builder builder;

    @GetMapping("/test/webClient")
    public Mono<String> test() {

        String url = "http://alibaba-nacos-discovery-server/hello?name=alibaba-nacos-discovery-client-common";

        Mono<String> response = builder.build()
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class);

        return response;

    }

}
