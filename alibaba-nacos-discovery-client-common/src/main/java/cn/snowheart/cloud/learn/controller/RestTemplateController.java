package cn.snowheart.cloud.learn.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author liuwanxiang
 * @version 2019/04/15
 */
@Slf4j
@RestController
public class RestTemplateController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/test/restTemplate")
    public String test() {

        String url = "http://alibaba-nacos-discovery-server/hello?name=alibaba-nacos-discovery-client-common";
        String response = restTemplate.getForObject(url, String.class);
        return "Invoke : " + url + ", response : " + response;

    }

}
