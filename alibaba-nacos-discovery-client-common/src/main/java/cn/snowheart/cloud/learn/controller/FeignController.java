package cn.snowheart.cloud.learn.controller;

import cn.snowheart.cloud.learn.client.ServerClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuwanxiang
 * @version 2019/04/15
 */
@Slf4j
@RestController
public class FeignController {

    @Autowired
    private ServerClient serverClient;

    @GetMapping("/test/feign")
    public String test() {

        return serverClient.hello("alibaba-nacos-discovery-client-common");

    }

}
