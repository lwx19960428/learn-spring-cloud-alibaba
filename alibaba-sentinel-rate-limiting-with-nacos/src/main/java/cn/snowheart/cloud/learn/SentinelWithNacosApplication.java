package cn.snowheart.cloud.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuwanxiang
 * @version 2019/04/19
 */
@SpringBootApplication
public class SentinelWithNacosApplication {

    public static void main(String[] args) {

        SpringApplication.run(SentinelWithNacosApplication.class, args);

    }

}
