# Spring Cloud Alibaba系列学习工程

> 参考[程序猿DD](http://blog.didispace.com/spring-cloud-learning/)的博客进行学习，工程为自己学习过程中的代码记录。

## Nacos 服务发现和配置中心

1. [Spring Cloud Alibaba基础教程：使用Nacos实现服务注册与发现](http://blog.didispace.com/spring-cloud-alibaba-1/)

    - alibaba-nacos-discovery-server 服务提供者
    - alibaba-nacos-discovery-client-common TestController类

2. [Spring Cloud Alibaba基础教程：支持的几种服务消费方式（RestTemplate、WebClient、Feign）](http://blog.didispace.com/spring-cloud-alibaba-2/)

    - alibaba-nacos-discovery-server 服务提供者
    - alibaba-nacos-discovery-client-common    
        - RestTemplateController类
        - web实现
        - 依赖`spring-boot-starter-web`
        
    - alibaba-nacos-discovery-client-common
        - WebClientController类
        - WebFlux实现
        - 依赖`spring-boot-starter-webflux`

    - alibaba-nacos-discovery-client-common
        - FeignController类
        - Feign实现
        - 依赖`spring-cloud-starter-openfeign`

3. [Spring Cloud Alibaba基础教程：使用Nacos作为配置中心](http://blog.didispace.com/spring-cloud-alibaba-3/)

    - alibaba-nacos-config-client
        
        **TODO & 疑问**
        
        `@RefreshScope`注解在Service上时，项目可以启动成功，并可以获得相应`@Value`的值，且可以正常刷新值；
        
        `@RefreshScope`注解在Controller上时，项目中无法获取相应`@Value`值，且无法刷新；
        
        当Controller上不加`@RefreshScope`时，项目中可以获取相应`@Value`值，但无法刷新；

4. [Spring Cloud Alibaba基础教程：Nacos配置的加载规则详解](http://blog.didispace.com/spring-cloud-alibaba-nacos-config-1/)

    - alibaba-nacos-config-client

5. [Spring Cloud Alibaba基础教程：Nacos配置的多环境管理](http://blog.didispace.com/spring-cloud-alibaba-nacos-config-2/)

    - alibaba-nacos-config-client

6. [Spring Cloud Alibaba基础教程：Nacos配置的多文件加载与共享配置](http://blog.didispace.com/spring-cloud-alibaba-nacos-config-3/)

    - alibaba-nacos-config-client

7. [Spring Cloud Alibaba基础教程：Nacos的数据持久化](http://blog.didispace.com/spring-cloud-alibaba-4/)

    Nacos连接Mysql数据库，涉及配置文件`%NACOS_HOME%/conf/application.properties`，增加如下内容

    ```
    spring.datasource.platform=mysql
    
    db.num=1
    db.url.0=jdbc:mysql://localhost:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
    db.user=username
    db.password=password
    ```

8. [Spring Cloud Alibaba基础教程：Nacos的集群部署](http://blog.didispace.com/spring-cloud-alibaba-5/)

    - 除修改nacos配置外，仍需要依赖Nginx等的负载均衡

## Sentinel 分布式系统的流量防卫兵

1. [Spring Cloud Alibaba基础教程：使用Sentinel实现接口限流](http://blog.didispace.com/spring-cloud-alibaba-sentinel-1/)

    - alibaba-sentinel-rate-limiting
    
        - Hello World项目
        
2. [Spring Cloud Alibaba基础教程：Sentinel使用Nacos存储规则](http://blog.didispace.com/spring-cloud-alibaba-sentinel-2-1/)

    - alibaba-sentinel-rate-limiting-with-nacos

        - 这种组合，现阶段仅可通过nacos来修改2个里面的配置。无法通过sentinel修改后同步nacos配置

3. [Spring Cloud Alibaba基础教程：Sentinel使用Apollo存储规则](http://blog.didispace.com/spring-cloud-alibaba-sentinel-2-2/)

    - **TODO 尚未运行成功，暂不提交代码**




## 端口规划

| 端口 | 用处 |
| --- | ---  |
| 8848 | NacOS单节点端口 |
| 8841-8843 | NacOS集群各节点端口 |
| 8844 | NacOS集群负载均衡端口 |
| 8858 | Sentinel DashBoard端口 |
| 8101-8109 | alibaba-nacos-discovery-server |
| 8111-8119 | alibaba-nacos-discovery-client-common |
| 8121-8129 | alibaba-nacos-config-client |
| 8131-8139 | alibaba-sentinel-rate-limiting |
| 8141-8149 | alibaba-sentinel-rate-limiting-with-nacos |

- 同一项目部署多个节点时，使用同一端口段内其他端口
