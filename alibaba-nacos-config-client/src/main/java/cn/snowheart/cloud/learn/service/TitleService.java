package cn.snowheart.cloud.learn.service;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @author liuwanxiang
 * @version 2019/04/15
 */
@RefreshScope
@Component
@Data
public class TitleService {

    @Value("${blog.title:}")
    String title;

    @Value("${server.port}")
    Integer port;

    @Value("${actuator.name:}")
    String actuatorName;

    @Value("${log.name:}")
    String logName;

}
