package cn.snowheart.cloud.learn.controller;

import cn.snowheart.cloud.learn.service.TitleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author liuwanxiang
 * @version 2019/04/15
 */
@Slf4j
@RestController
public class TitleController {

    @Autowired
    private TitleService titleService;

    @GetMapping("/blog/title")
    private String title() {
        log.info("title is {}", titleService.getTitle());
        log.info("port is {}", titleService.getPort());
        log.info("actuator name is {}", titleService.getActuatorName());
        log.info("log name is {}", titleService.getLogName());
        return titleService.getTitle();
    }

}
